UK Distance Calculator

REST service that will return geographic distance between two postal codes in UK
Result to a valid request must be a JSON document that contains the following
information:
• For both locations, the postal code, latitude and longitude (both in degrees);
• The distance between the two locations (in kilometers);
• A fixed string 'unit' that has the value "km";
For postal codes lookup: use the following data.
http://www.freemaptools.com/download-uk-postcode-lat-lng.htm
Distance is calculated using method provided at the end of this document.

Write REST service that will update coordinates for some postal code

Bonus​ : 
- Unit tests
- Authentication - restrict the service to those who know a username/password 
combination;

Technologies: 
- Spring boot
- Spring data
- For tests - junit and mockito 
- PostgreSQL database 