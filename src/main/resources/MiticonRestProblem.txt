REST API PROBLEM

Write REST service that will return geographic distance between two postal codes in UKResult to a valid request
must be a JSON document that contains the followinginformation:
• For both locations, the postal code, latitude and longitude (both in degrees);
• The distance between the two locations (in kilometers);
• A fixed string 'unit' that has the value "km";

For postal codes lookup: use the following data.http://www.freemaptools.com/download-uk-postcode-lat-lng.htm
Distance is calculated using method provided at the end of this document.

Write REST service that will update coordinates for some postal code

Bonus​:
- Unit tests-Authentication
- restrict the service to those who know a username/passwordcombination;

Technologies:-Spring boot-Spring data-For tests
- junit and mockito-PostgreSQL database

CODE FOR DISTANCE CALCULATING:
This bit of Java code computes (an approximation of) the distance between two points on theplanet,
given as long/lat pairs (in degrees).
private final static double EARTH_RADIUS = 6371; // radius in kilometers
private double calculateDistance(double latitude, double longitude, double latitude2, doublelongitude2) {
// Using Haversine formula! See Wikipedia;
double lon1Radians = Math.toRadians(longitude);
double lon2Radians = Math.toRadians(longitude2);
double lat1Radians = Math.toRadians(latitude);
double lat2Radians = Math.toRadians(latitude2);
double a = haversine(lat1Radians, lat2Radians) + Math.cos(lat1Radians) *Math.cos(lat2Radians) *haversine(lon1Radians, lon2Radians);
double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
return (EARTH_RADIUS * c);}

private double haversine(double deg1, double deg2) {
return square(Math.sin((deg1 - deg2) / 2.0));}
private double square(double x) {return x * x;}