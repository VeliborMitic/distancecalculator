package com.vex.distancecalc.repository;

import com.vex.distancecalc.entity.Town;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TownRepository extends JpaRepository<Town, String> {
  

  Town findByPostcode(String postcode);

}
