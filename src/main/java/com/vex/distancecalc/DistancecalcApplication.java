package com.vex.distancecalc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {"com.vex.distancecalc.entity"})
public class DistancecalcApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistancecalcApplication.class, args);
    }
}
