package com.vex.distancecalc.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends DistanceCalculatorException{

    public ResourceNotFoundException(String exception){
        super(exception);
    }

}
