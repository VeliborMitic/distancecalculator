package com.vex.distancecalc.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NotValidPostalcodeException extends PostalCodeException {
    public NotValidPostalcodeException() {
        super();
    }

    public NotValidPostalcodeException(String exception) {
        super(exception);
    }


}
