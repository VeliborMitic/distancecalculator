package com.vex.distancecalc.exceptions;

public class DistanceCalculatorException extends RuntimeException {

    public DistanceCalculatorException() {
    }

    public DistanceCalculatorException(String exception) {
        super(exception);
    }

}
