package com.vex.distancecalc.exceptions;

public class CoordinateException extends DistanceCalculatorException {

    public CoordinateException(String exception) {
        super(exception);
    }

    }

