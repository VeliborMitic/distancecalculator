package com.vex.distancecalc.dto;

import com.vex.distancecalc.entity.Town;
import com.vex.distancecalc.util.RequestValidation;

import javax.validation.constraints.NotNull;

public class UpdateCoordinatesResponse {
    @NotNull
    private double latitude;
    @NotNull
    private double longitude;

    public UpdateCoordinatesResponse() {
    }

    public UpdateCoordinatesResponse(double latitude, double longitude) {
        RequestValidation.latitudeValidation(latitude);
        this.latitude = latitude;
        RequestValidation.longitudeValidation(longitude);
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "New coordinates{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
