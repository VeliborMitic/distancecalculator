package com.vex.distancecalc.dto;

import javax.validation.constraints.NotNull;

public class DistanceResponse {

    @NotNull
    private String postalCode1;
    @NotNull
    private String postalCode2;
    @NotNull
    private String distance;

    private String unit;

    public DistanceResponse(String postalCode1, String postalCode2, String distance, String unit) {
        this.postalCode1 = postalCode1;
        this.postalCode2 = postalCode2;
        this.distance = distance;
        this.unit = unit;
    }

    public DistanceResponse(@NotNull String postalCode1, @NotNull String postalCode2, @NotNull String distance) {
        this.postalCode1 = postalCode1;
        this.postalCode2 = postalCode2;
        this.distance = distance;
    }

    public String getPostalCode1() {
        return postalCode1;
    }

    public void setPostalCode1(String postalCode1) {
        this.postalCode1 = postalCode1;
    }


    public String getPostalCode2() {
        return postalCode2;
    }
    public void setPostalCode2(String postalCode2) {
        this.postalCode2 = postalCode2;
    }

    public String getDistance() {
        return distance;
    }
    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

}
