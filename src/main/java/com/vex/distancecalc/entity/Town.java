package com.vex.distancecalc.entity;

import com.vex.distancecalc.util.RequestValidation;

import javax.persistence.*;

@Entity
@Table(name = "postcodelatlng")
public class Town {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String postcode;
    private double latitude;
    private double longitude;


    public Town() {
    }

    public Town(String postcode, double latitude, double longitude) {
        this.postcode = postcode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Town(Long id, String postcode, double latitude, double longitude) {
        this.id = id;
        this.postcode = postcode;
        this.latitude = latitude;
        this.longitude = longitude;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        RequestValidation.isValidPostcode(postcode);
        this.postcode = postcode;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        RequestValidation.longitudeValidation(longitude);
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        RequestValidation.latitudeValidation(latitude);
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Postal code: " + postcode +
                ", Longitude: " + longitude +
                ", Latitude: " + latitude;
    }
}
