package com.vex.distancecalc.service;

import com.vex.distancecalc.dto.DistanceResponse;
import com.vex.distancecalc.dto.UpdateCoordinatesResponse;
import com.vex.distancecalc.entity.Town;
import com.vex.distancecalc.exceptions.NotValidPostalcodeException;
import com.vex.distancecalc.exceptions.ResourceNotFoundException;
import com.vex.distancecalc.repository.TownRepository;
import com.vex.distancecalc.util.DistanceCalcUtil;
import com.vex.distancecalc.util.RequestValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;


@Service
@Transactional
public class TownServiceImpl implements TownService {

    @Autowired
    private TownRepository townRepository;

    @Override
    public DistanceResponse calculateDistanceBetweenTwo(String postalCode1, String postalCode2) throws ResourceNotFoundException, NotValidPostalcodeException {

        if (!RequestValidation.isValidPostcode(postalCode1) || !RequestValidation.isValidPostcode(postalCode2)
                || postalCode1 == null || postalCode2 == null){
            throw new NotValidPostalcodeException("Not valid UK postalcode");
        }
        Town locationOne = townRepository.findByPostcode(postalCode1);
        Town locationTwo = townRepository.findByPostcode(postalCode2);

        if (locationOne == null || locationTwo == null){
            throw new ResourceNotFoundException("Location not found!");
        }

        double resultDistance = DistanceCalcUtil.calculateDistance(locationOne.getLatitude(), locationOne.getLongitude(), locationTwo.getLatitude(), locationTwo.getLongitude());
        String resultDistanceString = Double.toString(resultDistance);
        DistanceResponse distanceResponse = new DistanceResponse(locationOne.toString(), locationTwo.toString(), resultDistanceString, new String("km"));

        return distanceResponse;
    }

    @Override
    public UpdateCoordinatesResponse updateLocationCoordinates(UpdateCoordinatesResponse location, String postalCode) throws ResourceNotFoundException, NotValidPostalcodeException {

        if (!RequestValidation.isValidPostcode(postalCode)){
            throw new NotValidPostalcodeException("Not valid UK postalcode");
        }
        Town currentLocation = townRepository.findByPostcode(postalCode);
        if (currentLocation == null) throw new ResourceNotFoundException("Location with postalcode \"" + postalCode + "\" not found!!!");

        currentLocation.setLatitude(location.getLatitude());
        currentLocation.setLongitude(location.getLongitude());
        townRepository.save(currentLocation);

        UpdateCoordinatesResponse updateCoordinatesResponse = new UpdateCoordinatesResponse(currentLocation.getLatitude(), currentLocation.getLongitude());

        return updateCoordinatesResponse;
    }

    @Override
    public Town findTownByPostalCode(String postcode) throws NotValidPostalcodeException {

        if (!RequestValidation.isValidPostcode(postcode)){
            throw new NotValidPostalcodeException("Postal code \"" + postcode + "ˇ\" is not valid UK postalcode!!!!");
        }
        Town town = townRepository.findByPostcode(postcode);
               if (town == null) throw new ResourceNotFoundException("Location with postalcode \"" + postcode + "\" not found!!!");


        return town;
    }



}
