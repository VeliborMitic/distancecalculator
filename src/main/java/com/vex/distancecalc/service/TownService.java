package com.vex.distancecalc.service;

import com.vex.distancecalc.dto.DistanceResponse;
import com.vex.distancecalc.dto.UpdateCoordinatesResponse;
import com.vex.distancecalc.entity.Town;
import com.vex.distancecalc.exceptions.NotValidPostalcodeException;
import com.vex.distancecalc.exceptions.ResourceNotFoundException;

public interface TownService {

    DistanceResponse calculateDistanceBetweenTwo (String postalCode1, String postalCode2) throws ResourceNotFoundException, NotValidPostalcodeException;

    UpdateCoordinatesResponse updateLocationCoordinates (UpdateCoordinatesResponse location, String postalCode) throws ResourceNotFoundException, NotValidPostalcodeException;

    Town findTownByPostalCode(String postalcode) throws NotValidPostalcodeException;
}
