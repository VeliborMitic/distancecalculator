package com.vex.distancecalc.util;

import com.vex.distancecalc.exceptions.CoordinateException;
import com.vex.distancecalc.exceptions.NotValidPostalcodeException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RequestValidation {

    public static boolean isValidPostcode(String postcode)throws NotValidPostalcodeException {
        String regex = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(postcode);
        return m.matches();
    }

    public static void latitudeValidation(double x) throws CoordinateException {
        if (x < 40){
            throw new CoordinateException("Latitude for UK must be larger then 40 degrees!");
        }
    }

    public static void longitudeValidation(double x) throws CoordinateException {
        if (x < -5 || x > 5){
            throw new CoordinateException("Longitude for UK must be in range from -5 to 5! ");
        }
    }


}
