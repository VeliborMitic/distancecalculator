package com.vex.distancecalc.controller;

import com.vex.distancecalc.dto.UpdateCoordinatesResponse;
import com.vex.distancecalc.exceptions.NotValidPostalcodeException;
import com.vex.distancecalc.exceptions.ResourceNotFoundException;
import com.vex.distancecalc.service.TownServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
public class TownController {

    @Autowired
    private TownServiceImpl townService;

    @GetMapping("/distance/{postcode1}/{postcode2}")
    public ResponseEntity<Object> findDistanceBetweenTwo (@PathVariable String postcode1, @PathVariable String postcode2) throws NotValidPostalcodeException {
        return ResponseEntity.ok(townService.calculateDistanceBetweenTwo(postcode1, postcode2));
    }

    @PutMapping("/update/{postcode}")
    public ResponseEntity<Object> updateTown(@RequestBody @Valid UpdateCoordinatesResponse location, @PathVariable String postcode) throws NotValidPostalcodeException {
        return ResponseEntity.ok(townService.updateLocationCoordinates(location, postcode));
    }

    @GetMapping("/{postcode}")
    public ResponseEntity<Object> retrieveTown(@PathVariable String postcode) throws NotValidPostalcodeException, ResourceNotFoundException {
        return ResponseEntity.ok(townService.findTownByPostalCode(postcode));
    }




}
