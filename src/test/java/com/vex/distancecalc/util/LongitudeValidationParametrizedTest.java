package com.vex.distancecalc.util;

import com.vex.distancecalc.exceptions.CoordinateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class LongitudeValidationParametrizedTest {

    private double longitude;

    public LongitudeValidationParametrizedTest(double longitude) {
        this.longitude = longitude;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {27.14870708},
                {13.6854655},
                {-7.654654}
        });
    }

    @Test(expected = CoordinateException.class)
    public void wrongLatitudeCoordinateTest() {
        RequestValidation.longitudeValidation(longitude);
    }

}
