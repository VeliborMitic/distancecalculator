package com.vex.distancecalc.util;

import com.vex.distancecalc.exceptions.CoordinateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class LatitudeValidationParametrizedTest {

    private double latitude;

    public LatitudeValidationParametrizedTest(double latitude) {
        this.latitude = latitude;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {2.14870708},
                {3.6543551},
                {0.98785413}
        });
    }

    @Test(expected = CoordinateException.class)
    public void wrongLatitudeCoordinateTest() {
    RequestValidation.latitudeValidation(latitude);
    }


}
