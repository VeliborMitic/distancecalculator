package com.vex.distancecalc.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class DistanceCalcUtilTest {

    @Test
    public void correctResultDistanceCalcTest(){
        //Prepare
        double expected = 2.350757074301658;

        //Excercise
        double actual = DistanceCalcUtil.calculateDistance(57.147428, -2.1472662, 57.151797, -2.185398);

        //Verify
        assertEquals(actual, expected, 0.0);
    }

    @Test
    public void incorrectResultDistanceCalcTest(){
        //Prepare
        double expected = 3.350757074301658;

        //Excercise
        double actual = DistanceCalcUtil.calculateDistance(57.147428, -2.1472662, 57.151797, -2.185398);

        //Verify
        assertNotEquals(actual, expected, 0.0);
    }

}
