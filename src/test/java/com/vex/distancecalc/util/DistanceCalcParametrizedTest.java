package com.vex.distancecalc.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class DistanceCalcParametrizedTest {

    private double latitudeOne;
    private double longitudeOne;
    private double latitudeTwo;
    private double longitudeTwo;
    private double expectedResult;


    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {57.14870708, -2.097806027, 57.10155692, -2.268485752, 11.559144516398517}, //AB12 9SP, AB14 0TQ
                {57.147428, -2.1472662, 57.151797, -2.185398, 2.350757074301658}, //AB15 5HB, AB15 6NA
                {57.14416516, -2.114847768, 57.13562422, -2.175239142, 3.7653435719633057}, //AB10 1XG/AB15 8BA
                {57.15400596, -2.22440188, 57.17384698, -2.181539371, 3.3979838037282972} //AB15 8UF/AB21 9DA
            });
        }

    public DistanceCalcParametrizedTest(double latitudeOne, double longitudeOne, double latitudeTwo, double longitudeTwo, double expectedResult) {
        this.latitudeOne = latitudeOne;
        this.longitudeOne = longitudeOne;
        this.latitudeTwo = latitudeTwo;
        this.longitudeTwo = longitudeTwo;
        this.expectedResult = expectedResult;
    }

    @Test
    public void test() {
        Assert.assertEquals("Test FAIL", expectedResult, DistanceCalcUtil.calculateDistance
                (latitudeOne, longitudeOne, latitudeTwo, longitudeTwo), 0.001);
    }
}
