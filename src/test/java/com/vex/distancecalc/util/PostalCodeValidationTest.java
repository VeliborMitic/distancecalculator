package com.vex.distancecalc.util;

import com.vex.distancecalc.exceptions.NotValidPostalcodeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
public class PostalCodeValidationTest {

    @Test
    public void testWithValidPostcode() throws NotValidPostalcodeException {
        //Prepare
        String testCode = "AB15 6NA";

        //Excercise
        boolean result = RequestValidation.isValidPostcode(testCode);

        //Verify
        assertTrue(result);
    }

    @Test
    public void testWrongValidPostcode() throws NotValidPostalcodeException{
        //Prepare
        String testCode = "11223344";

        //Excercise
        boolean result = RequestValidation.isValidPostcode(testCode);

        //Verify
        assertFalse(result);
    }

}
