package com.vex.distancecalc.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(Parameterized.class)
public class PostalCodeValidationParametrizedTest {

    private String postcode;
    private boolean expected;


    public PostalCodeValidationParametrizedTest(String postcode, boolean expected) {
        this.postcode = postcode;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"1111 111 ", false},
                {"11111111", false},
                {"AB99_9AB", false},
                {"AB99/9AB", false},
                {"", false},
                {"        ", false},
                {"AB", false},
                {"AB10", false},
                {"AB10 ", false},
                {"AB10 A", false},
                {"AB10 2", false},
                {"AB10 2A", false},
                {"AB10 AA", false},
                {"AA10 111", false},
                {"0000 1AA", false},
                {"0000 AAA", false},
                {"ABAA 1AA", false},
                {"ABAA 111", false},
                {"+A12 1ZZ", false},
                {"A-12 1ZZ", false},
                {"AC1* 1ZZ", false},
                {"AC%2 1ZZ", false},
                {"AC92 #ZZ", false},
                {"ZC92 9&Z", false},
                {"ZZ99 1 Z", false},
                {"+-/* !@#", false},
                {"AB10 1XG", true},
                {"AA00 0AA", true},
                {"ZZ99 9ZZ", true},
                {"aa11 1ZZ", false},
                {"aa11 1zz", false},
                {"AA11 1zz", false},
                {"aV11 1Az", false},
                {"aV11 1aZ", false},
                {"Av11 1Az", false},
                {"Av11 1Az", false},
                {" AB11 1AB", false},
                {"AB11 1AB ", false},
        });
    }
    @Test
    public void isValidPostcode() {
        boolean actual= RequestValidation.isValidPostcode(postcode);
        assertThat(actual, equalTo(expected));
    }
}
