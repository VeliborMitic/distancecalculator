package com.vex.distancecalc.service;

import com.vex.distancecalc.dto.DistanceResponse;
import com.vex.distancecalc.dto.UpdateCoordinatesResponse;
import com.vex.distancecalc.entity.Town;
import com.vex.distancecalc.exceptions.CoordinateException;
import com.vex.distancecalc.exceptions.NotValidPostalcodeException;
import com.vex.distancecalc.exceptions.ResourceNotFoundException;
import com.vex.distancecalc.repository.TownRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TownServiceTest {

    @Mock
    TownRepository townRepository;

    @InjectMocks
    TownServiceImpl townService;

    @Test
    public void testCalculateDistanceBetweenTwo() throws NotValidPostalcodeException{

        //Arrange
        Town location1 = new Town("AB15 5HB", 57.147428,-2.1472662);
        Town location2 = new Town("AB15 6NA", 57.151797,-2.185398);

        when(townRepository.findByPostcode("AB15 5HB")).thenReturn(location1);
        when(townRepository.findByPostcode("AB15 6NA")).thenReturn(location2);

        //Act
        DistanceResponse actual = townService.calculateDistanceBetweenTwo(location1.getPostcode(),location2.getPostcode());
        DistanceResponse expected = new DistanceResponse(location1.toString(),location2.toString(), Double.toString(2.350757074301658));

        //Assert
        assertEquals(Double.parseDouble(actual.getDistance()), Double.parseDouble(expected.getDistance()),0.0001);

}

    @Test(expected = NotValidPostalcodeException.class)
    public void testCalculateDistanceWithInvalicPostalCode() throws NotValidPostalcodeException{

        //Arrange
        Town location1 = new Town("AB1111 5HB", 57.147428,-2.1472662);
        Town location2 = new Town("AB15 6NA", 57.151797,-2.185398);

       /* when(townRepository.findByPostcode("AB15 5HB")).thenReturn(location1);
        when(townRepository.findByPostcode("AB15 6NA")).thenReturn(location2);*/

        //Act
        DistanceResponse actual = townService.calculateDistanceBetweenTwo(location1.getPostcode(),location2.getPostcode());
        DistanceResponse expected = new DistanceResponse(location1.toString(),location2.toString(), Double.toString(2.350757074301658));

        //Assert
        assertEquals(Double.parseDouble(actual.getDistance()), Double.parseDouble(expected.getDistance()),0.0001);

}

    @Test(expected = ResourceNotFoundException.class)
    public void testCalculateDistanceWhenPostalCodeDoesNotExist()throws NotValidPostalcodeException{

        //Arrange
        Town location1 = new Town("ZZ15 5HB", 57.147428,-2.1472662);
        Town location2 = new Town("AB15 6NA", 57.151797,-2.185398);

        /*when(townRepository.findByPostcode("AB15 5HB")).thenReturn(location1);
        when(townRepository.findByPostcode("AB15 6NA")).thenReturn(location2);*/

        //Act
        DistanceResponse actual = townService.calculateDistanceBetweenTwo(location1.getPostcode(),location2.getPostcode());
        DistanceResponse expected = new DistanceResponse(location1.toString(),location2.toString(),Double.toString(746.485612617802));

        assertEquals(Double.parseDouble(actual.getDistance()), Double.parseDouble(expected.getDistance()),0.0001);
}

    @Test
    public void testUpdateCoordinates() throws NotValidPostalcodeException {
        //Arrange
        Town locationOne = new Town("AB10 1XG", 57.14,-2.114);
        UpdateCoordinatesResponse expected = new UpdateCoordinatesResponse(57.14,-2.114);
        String postcode = "AB10 1XG";
        when(townRepository.findByPostcode("AB10 1XG")).thenReturn(locationOne);
        //Act
        UpdateCoordinatesResponse actual = townService.updateLocationCoordinates(expected, postcode);
        //Assert
        assertEquals(actual.getLongitude(), expected.getLongitude(),0.0);
        assertEquals(actual.getLatitude(), expected.getLatitude(),0.0);

    }
    @Test
    public void testUpdateCoordinatesWithInvalidLongitude() throws NotValidPostalcodeException {
        //Arrange
        Town location1 = new Town("AB10 1XG" , 57.14,-10.114);
        UpdateCoordinatesResponse expected = new UpdateCoordinatesResponse(57.14,-2.114);
        String postcode = "AB10 1XG";
        when(townRepository.findByPostcode("AB10 1XG")).thenReturn(location1);
        //Act
        UpdateCoordinatesResponse actual = townService.updateLocationCoordinates(expected, postcode);
        //Assert
        assertEquals(actual.getLongitude(), expected.getLongitude(),0.0);
        assertEquals(actual.getLatitude(), expected.getLatitude(),0.0);

    }


    @Test
    public void testFindTownByValidPostalCode() throws NotValidPostalcodeException, ResourceNotFoundException{

        //Arrange
        Town expected = new Town("AB21 7NG", 57.20437174, -2.176128);
        expected.setId(45L);
        when(townRepository.findByPostcode("AB21 7NG")).thenReturn(expected);

        //Act
        Town actual = townService.findTownByPostalCode("AB21 7NG");

        //Assert
        assertEquals(expected, actual);

    }

    @Test (expected = NotValidPostalcodeException.class)
    public void testFindTownByInvalidPostalCode() throws CoordinateException, NotValidPostalcodeException {

       /* //Arrange
        Town expected = new Town( "AB21 7NG",57.20437174,-2.176128 );
        expected.setId(45L);
        when(townRepository.findByPostcode("AB21 7NG")).thenReturn(expected);*/

        //Act
        Town actual = townService.findTownByPostalCode("AB217NG");

        /*//Assert
        assertEquals(expected, actual);*/
    }

}
